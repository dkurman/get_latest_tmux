# Simple script to fetch and install latest tmux for Ubuntu

The version fetched with apt-get is not up to date.  Some of the tips in the [tmux 2](https://pragprog.com/book/bhtmux2/tmux-2) book depend on the newer 2.3 version.  